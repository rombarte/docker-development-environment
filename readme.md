# Docker Development Environment
#### My predefined development environment with Docker for PHP programming
## Environment content
1. Apache
2. PHP 7.3
3. MySQL 5.7
## Build environment
```
docker-compose build --force-rm --no-cache
```
## Start environment
```
docker-compose up --detach
```
## Stop environment
```
docker-compose down
```
## Licence
All files in this repository are available under the MIT license. You can find more information and a link to the license content on the [Wikipedia](https://en.wikipedia.org/wiki/MIT_License) page.